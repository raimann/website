#!/bin/bash

# Parameters

theme='ananke'

overrides_dst=(\
    'layouts/index.html' \
    'layouts/_default/single.html' \
    'layouts/partials/site-scripts.html' \
    )

# Code:

git submodule update --remote --merge

overrides_src_dir="themes/${theme}"

timestamp=$(date +%s)

for dst_fname in ${overrides_dst[*]}; do
    backup_fname="${dst_fname}.${timestamp}.backup"
    src_fname="${overrides_src_dir}/${dst_fname}"
    
    mv "${dst_fname}" "${backup_fname}"
    cp "${src_fname}" "${dst_fname}"
    cmp -s "${backup_fname}" "${src_fname}" || echo "${dst_fname} has changed."
    
done

exit 0

