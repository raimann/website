# website

Website for the [computational-acoustics](https://gitlab.com/computational-acoustics) project.

# How to clone:

```bash
 git clone --recurse-submodules git@gitlab.com:computational-acoustics/website.git
```

Or

```bash
git clone git@gitlab.com:computational-acoustics/website.git && cd website
git submodule update --init --recursive
```

