---
title:  "Modelling Acoustics With Open Source Software"
date:   2018-10-27 20:00:00 +0100
draft: false
featured_image: "/site_img/featured_image.png"
description: "A Look at the Ecosystem"
categories: ["Software"]
tags: ["Introductory", "Software"]
---

One of the backbones of the scientific paradigms is the repeatability of results. Ideally, all software used for scientific research should be open source, so to allow complete repeatability of data analysis and models. And in fact, a lot of this software is indeed open source, and a lot of this software can be used to model acoustic phenomena.

What follows below is a panoramic overview of the scientific open source software ecosystem. This overview not 100% comprehensive. In fact, perhaps it is not even 10% comprehensive. Neither it reports software that pertains acoustic simulation only, even though a greater emphasis is given on this. However, it should represent a decent picture of how many tools are available for modelling complex physics, as well as creating all sorts of technical computing projects.

# Technical Computing Languages

There are many technical computing languages, or languages that can be useful for technical/scientific computing. The most commonly used are:

* [GNU Octave](https://www.gnu.org/software/octave/). This is very useful also for having compatible syntax to that of Matlab, however it tends to be on the slower side (see the [benchmarks](https://julialang.org/benchmarks/) released by the Julia developers).
* [Python](https://www.python.org/). Python is very nice to code with, and there are tons of scientific packages for it. Moreover, certain numerical software programs and frameworks allow Python scripting (ParaView, as an example, see below) or have Python interfaces. So, it is a language that it is worth having a grasp of.
* [Julia](https://julialang.org/) Julia is a new exciting language that feels like what would happen if Octave and Python had a baby. One if its main features is that it can reach C-like speed while being a very high level language. Also worth to mention the plethora of packages already existing for the language, making it a very powerful platform already.
* [Maxima](http://maxima.sourceforge.net/) is a very useful computer algebra system that can be used for symbolic computation. The GUI front-end [wxMaxima](https://wxmaxima-developers.github.io/wxmaxima/) is one of the most convenient ways to use it.
* [Modelica](https://www.modelica.org/) is a language for multiphysics modelling. An entire open-source modelling environment, [OpenModelica](https://www.openmodelica.org/) is based on it. Publications exist in which Modelica was used to model acoustics (such as [this](https://pdfs.semanticscholar.org/1918/2c2cb8967271953073736e457f525251623e.pdf)).

Note that this list is not exhaustive and many more examples are to be found, such as (but not limited to) [R](https://www.r-project.org/),  [Scilab](http://www.scilab.org/), [SageMath](https://www.sagemath.org/) as well as a plethora of open source scientific frameworks for other languages such as, for example, [ROOT](https://root.cern.ch/).

# Physical Modelling DSP

The [FAUST](https://faust.grame.fr/) programming language is a very powerful language to program real-time DSP algorithms. In its library there are numerous functions for physical modelling of music instruments. Moreover, it has an entire [Physical Modelling Toolkit](https://ccrma.stanford.edu/~rmichon/pmFaust/).

Again, FAUST is not the only open source domain specific language for DSP. There is also [ARRP](http://arrp-lang.info/), for example.

# Numerical Simulation and Analysis Packages

## Numerical Modelling and Solvers

These packages are suites for numerical modelling, using various methods.

* [Elmer FEM](https://www.csc.fi/web/elmer) is a multiphysical FEM solver which is very powerful, although not very user friendly. It has very good solvers for various acoustic governing equations. Being a multiphysics solver, it allows coupling different solvers.
* [AcouSTO](http://acousto.sourceforge.net/) is an interesting BEM solver for acoustics.
* [OpenPSTD](http://www.openpstd.org/index.html) is an acoustics simulation program that uses the Pseudo Spectral Time Domain Method. It integrates with Blender.
* [I-Simpa](http://i-simpa.ifsttar.fr/) is open source software designed to simulate 3D acoustic propagation.
* [EVERTims](https://evertims.github.io/) is an open source framework for real-time auralization in architectural acoustics and virtual reality. It works with Blender, even in realtime, and it has a Python API.
* [Mesh2HRTF](http://mesh2hrtf.sourceforge.net/) is a very interesting package for the numerical simulation of Head Related Transfer Functions (HRTF). Whilst open source itself, it depends on Matlab which is not open source.
* [Qucs](http://qucs.sourceforge.net/) is a nice, quite easy to use, circuit simulator. It can be used to simulate, of course, electronic circuits to use for audio applications, but it can also be used to build lumped acoustic models through electrical analogies (see [here](https://en.wikibooks.org/wiki/Engineering_Acoustics/Electro-Mechanical_Analogies) to read about electro-mechanical analogies).
* [The DREAM Toolbox](http://www.signal.uu.se/Toolbox/dream/index.shtml) and [Field II](http://field-ii.dk//) are two interesting open source Matlab/Octave toolboxes for the simulation of ultrasonic transducers arrays, with focus on medical imaging applications (especially Field II).
* [k-Wave](http://www.k-wave.org/) is a Matlab open source acoustics toolbox designed for time domain acoustic and ultrasound simulations in complex and tissue-realistic media. Whilst it is made for Matlab, it is [reported to work with Octave as well](http://www.k-wave.org/forum/forum/octave). A list of open source acoustic solvers, similar to this one, is reported on [this page](http://www.k-wave.org/acousticsoftware.php) of the k-Wave project website.
* [EOF-Library](https://eof-library.org/) is an open source coupling library for Elmer FEM and [OpenFOAM](https://openfoam.com/) simulation packages. OpenFOAM is a Compudational Fluid Dinamics (CDF) solver. The EOF-Library acoustics capabilities are not fully clear, but it is possible this software might be useful to simulate the intercoupling between flow and sound generation.
* [FOCUS](https://www.egr.msu.edu/~fultras-web/) is a free cross-platform ultrasound simulation tool that quickly and accurately calculates pressure fields generated by single transducers and phased arrays. Whilst this software appears very capable, like few other examples in this list it depends on Matlab, which is not itself open source.

Of course, there are actually many more solvers, such as [Calculix](http://www.calculix.de/), [Code_Aster](https://code-aster.org/spip.php?rubrique2), [GetFEM++](http://getfem.org/), [Freefem++](http://www.freefem.org/), [FEMM](http://www.femm.info/wiki/HomePage), [MOOSE](https://mooseframework.inl.gov) and [ONELAB](http://onelab.info/) to cite a few. Those reported are the ones with the greatest capabilities for acoustics, or acoustic related phenomena modelling.

## Psychoacoustics Modelling and Analysis

There are good packages also in the domain of psychoacoustics:

* [Auditory Modelling Toolbox](http://amtoolbox.sourceforge.net/) is an open source Matlab/Octave toolbox for auditory modelling.
* [The Large Time-Frequency Analysis Toolbox](http://ltfat.github.io/) is a Matlab/Octave toolbox that provides many useful transforms for signal analysis that can be very useful to understand acoustic signals.
* [Psychtoolbox](http://psychtoolbox.org/) is another Matlab/Octave toolbox that can be used for realtime analysis of auditory stimuli.
* [CMUSphinx](https://cmusphinx.github.io) is an open source toolbox for speech recognition.
* [MoSQITo](https://github.com/Eomys/MoSQITo) is an open-source python package by [EOMYS](https://eomys.com/?lang=en) which provides a unified and modular development framework of key sound quality metrics with open-source object-oriented technologies.


# 3D Modelling and CAD

In order to solve a problem, normally one must first create its geometry first. There are really many programs out of there which can be used for this. The ones below are especially worth to mention due to many solvers providing interfaces and integration for them.

* [FreeCAD](https://www.freecadweb.org/) is very good to prepare geometries for FEM solving. Certain solvers, such as Calculix and ElmerFEM, have an interface for FreeCAD which integrates within its GUI.
* [Blender](https://www.blender.org/) is more for graphics, but certain solvers (such as AcouSTO) actually have plugins for Blender. Blender is another good example of software that allows internal Python scripting.

# Pre and Post Processing

In order to use any solver, our geometry needs to be prepossessed. Moreover, our results from the numerical solvers might need further postprocessing. There are many packages taht provide these functionality. Below are reported the most widely used and flexible.

* [gmsh](http://gmsh.info/) is a lightweight and effective pre- and postprocessor.
* [Salome Platform](http://www.salome-platform.org/) is a very feature rich pre- and postprocessor. Plugins exist that integrate solvers within it, such as the [ElmerFEM plugin](https://github.com/physici/ElmerSalomeModule). To be noted that there are many packages based on Salome, such as Salome-Meca, a modelling suite based on Salome Platform incorporating the Code_Aster solver.
* [ParaView](https://www.paraview.org/) is a very good postprocessor. Note that it is included as a module in Salome, so it can be used within Salome.

# Conclusion

This list was not exhaustive, and for every section there is plenty of good software that was not reported. However, it should give a sense of how vast the ecosystem is. Packages such as ElmerFEM, Salome and ParaView are very mature, while languages such as Python are the de-facto backbone of scientific research. This article should be able to give the reader a sense of perspective about the many useful scientific and technical tools.

{{< cc >}}

