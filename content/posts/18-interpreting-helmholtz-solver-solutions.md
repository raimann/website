---
title: "Interpreting Helmholtz Solver Solutions"
date: 2020-10-10T15:39:39+01:00
draft: false
featured_image: "/posts_res/18-interpreting-helmholtz-solver-solutions/featured.svg"
description: "The Physical Meaning of Helmhotz FEM Fields"
categories: ["Modelling"]
tags: ["Introductory", "Physics"]
---

In the previous episodes we obtained various numerical FEM solutions for Helmholtz problems. Although we discussed their meaning in the previous episodes it is useful to discuss that again more deeply in a single page, which can act as a quicker reference. This episode will focus on the Helmholtz solver solution fields, while other episodes will focus on the output of other solvers.

# The Physical Meaning of a Solution Field

The meaning of a solution field largely depends on the solver itself. Each solver solves for different things and provides the results in different ways. This is why we have to take a look at each solver on a case by case basis. The Helmholtz solver provides harmonic steady state solutions. We should first understand what that means.

# Steady State Harmonic Fields

Let's assume we excite our acoustic system with a sine wave, provided by a certain source (for example, a [pulsating sphere]({{< ref "/posts/8-the-pulsating-sphere.md" >}}) source). Let's flick the source on and let's leave the sine wave running. After several seconds (the actual amount of time depending on the system itself) the system will have achieved a steady state response, that is, the parameters describing the response of the system are not subject to time variation.

To understand this intuitively think about this: as soon as we activate the source we will see that the system needs some time to react, that is, it has a _transient_ response. During this transient time the amplitude of its response might vary, for example, going from $0$ to some value, and then falling back again, a few times. But give it some time and the amplitude will settle. That is steady state.

In the example above we used a sine wave input for our system. This is not a case, as most often we study _harmonic_ steady state response. This actually naturally arise due to steady state solutions being a product of a spatial part and an harmonic time part for most PDEs, as we seen in the [Acoustic Models of a Rectangular Room]({{< ref "/posts/5-acoustic-modes-of-a-rectangular-room.md" >}}) episode.

Let's now take a more quantitative look.

## The Helmholtz Solution Field

The first port of call to understand the physical meaning of any solver output is the solver documentation, in this case [The Helmholtz Model documentation](ephy-resource:///org/gnome/epiphany/pdfjs/web/#chapter.12) for ElmerFEM. Below we will use the same notation as in the documentation, but with some changes to add clarity.

Let's first define our domain as $\mathcal{D} \subset \mathbb{R}^{3}$. We will denote with $\mathbf{u}$ the generic point in $\mathcal{D}$. We summarise this as follows:

$$ \mathbf{u} = \begin{bmatrix} x \\\ y \\\ z \end{bmatrix} \in \mathcal{D} \subset \mathbb{R}^{3}$$

Where we denoted with $x$, $y$ and $z$ the Cartesian components of $\mathbf{u}$ (note that any coordinate system will work just the same, here we use Cartesian for simplicity).

The Helmholtz solver will provide us with a field $P$ defined as follows:

$$ P:\mathcal{D} \rightarrow \mathbb{C} $$

Through $P$, the elements $\mathbf{u}$ of $\mathcal{D}$ are associated to complex numbers:

$$ \mathbf{u} \in \mathcal{D} \mapsto P\left(\mathbf{u} \right) \in \mathbb{C}$$

That is, $P$ is a _scalar field_ defined over $\mathcal{D}$, as it assumes single scalar values for each point in $\mathcal{D}$ (albeit complex scalars).

By Reading the manual, we can see that $P$ concurs in defining the physical solution as follows:

$$ p\left(\mathbf{u}, t \right) = \Re \left[ P\left(\mathbf{u} \right) \exp \left( j \omega t\right) \right]$$

As we mentioned above, the steady state solution is a product of a spatial part and a temporal part. Here we see exactly that: $P$ is a function depending only on space, while $\exp \left( j \omega t\right)$ depends solely on time $t$, $\omega$ being the angular frequency at which we run the simulation and $j$ being the imaginary unit. In other words, the temporal part is just a complex sinusoidal wave.

$p$ represents the pressure disturbance in any point $\mathbf{u}$ of the domain $\mathcal{D}$ and any time $t$ at which the system is at steady state. Pressure disturbance, or acoustic pressure, is the pressure variation on top of the static pressure value of the fluid sustaining the acoustic wave propagation. Being physical acoustic pressure a real number we "encode" it as the real part of the product between spatial part and temporal part.

As a first conclusion we have then this: the field $P$, output of the Helmholtz solver, is the spatial part of the harmonic steady state solution.

## The Physical Meaning of $P$

By Looking at the equation above you might wrongly conclude that $P$ is a mere ingredient for calculating the actual physical solution and that its complex nature is just a mathematical convenience, as the final result only makes use of the real part. This is actually far from the truth, as $P$ essentially contains all the information that is useful for us.

To understand this, let's make the complex nature of $P$ more explicit. Being $P\left(\mathbf{u} \right)$ a complex number for each $\mathbf{u} \in \mathcal{D}$ let us denote with $M\left(\mathbf{u} \right)$ and $\Phi\left(\mathbf{u} \right)$ the magnitude and phase of this complex number respectively:

$$ M:\mathcal{D} \rightarrow \mathbb{R} $$

$$ \mathbf{u} \in \mathcal{D} \mapsto M\left(\mathbf{u} \right) = |P\left(\mathbf{u} \right)| \in \mathbb{R}$$

and 

$$ \Phi:\mathcal{D} \rightarrow \left(-\pi, \pi \right] $$

$$ \mathbf{u} \in \mathcal{D} \mapsto \Phi\left(\mathbf{u} \right) = \angle P\left(\mathbf{u} \right) \in \left(-\pi, \pi \right]$$

So:

$$ P\left(\mathbf{u} \right) = M\left(\mathbf{u} \right) \exp \left( j  \Phi\left(\mathbf{u} \right) \right)$$

Let's now compute the product of spatial and temporal parts:

$$ P\left(\mathbf{u} \right) \exp \left( j \omega t\right) = M\left(\mathbf{u} \right) \exp \left( j \left[ \Phi\left(\mathbf{u} \right) + \omega t \right] \right) = $$

$$ = M\left(\mathbf{u} \right) \left[ \cos \left( \Phi\left(\mathbf{u} \right) + \omega t \right) + j \sin 
\left( \Phi\left(\mathbf{u} \right) + \omega t \right) \right]$$

where we used the properties of the exponential functions and Euler's formula. So, to get the physical pressure disturbance field we only need to take the real part:

$$ p\left(\mathbf{u}, t \right) = M\left(\mathbf{u} \right) \cos \left( \Phi\left(\mathbf{u} \right) + \omega t \right) $$

As you can see, both the magnitude and phase of $P$ concur in defining the physical pressure disturbance field. We can then put the properties of $P$ in relation to the physical pressure disturbance.

### $P$ Magnitude

From the equation above it is evident that the magnitude of $P$ has the straightforward meaning of amplitude, i.e. $M\left(\mathbf{u} \right)$ tells us what the amplitude of the pressure disturbance is in any point of the domain, for every time at which the system is sustaining steady state vibrations at the angular frequency $\omega$. Clearly, the units of $M\left(\mathbf{u} \right)$ are $\text{Pa}$ (as ElmerFEM uses SI units). We can use this information as is, or use it to compute Sound Pressure Levels (SPL):

$$ \text{SPL}\left(\mathbf{u} \right) = 20 \log \left( \frac{M\left(\mathbf{u} \right)}{20 \mu \text{Pa}} \right) $$

or any other useful metric of amplitude we might need.

### $P$ Phase

The phase of $P$ has a slightly less straightforward meaning. The final pressure disturbance equation might have us to believe that $\Phi\left(\mathbf{u} \right)$ has the meaning of initial phase for each point of the domain, that is, the initial phase found at each point of the domain at $t=0$. However, this is not completely correct as it is not clear what we mean for $t=0$.

Think we were observing a physical system and decide to set the clock to $0$ when we are reasonably sure it has reached steady state. This means that at any time we set the clock to $0$ the pressure disturbance has already been vibrating for some time, and it will have then some unspecified phase already, coming from the transient and the arbitrary time we waited for after its conclusion (so to make sure we begin our observations at proper steady state). That is, for our physical system at steady state we would observe this:

$$ p\left(\mathbf{u}, t \right) = M\left(\mathbf{u} \right) \cos \left( \Phi\left(\mathbf{u} \right) + \omega t + \phi \right) $$

with $\phi$ an unknown phase factor unless we tracked the full evolution of the system up to the point we reset our clock. Clearly, the Helmholtz equation does not model any time evolution before steady state: it is like it is assuming steady state is everlasting and eternal. So we do not have any other information to decide what is the actual initial phase.

The simplest way to think about $\Phi\left(\mathbf{u} \right)$ is perhaps as phase gap between points in the domain: no matter what value of the time $t$ or arbitrary phase $\phi$, the phase gap between any two different points in the domain $\mathbf{u}\_{1}$ and $\mathbf{u}\_{2}$ will be:

$$ \Phi\left(\mathbf{u}\_{1} \right) - \Phi\left(\mathbf{u}\_{2} \right) $$

which, numerically, is best calculated as follows:

$$ \angle \frac{P\left(\mathbf{u}\_{1} \right)}{P\left(\mathbf{u}\_{2} \right)}$$

In other words, $\Phi\left(\mathbf{u} \right)$ tells us the relative phase between each point in the domain, for every time at which the system is sustaining steady state vibrations at the angular frequency $\omega$. Of course, the units of $\Phi$ are radiants.

# Conclusion

In this episode we looked at the field $P$ which the ElmerFEM Helmholtz Solver provides. We seen that it is a complex scalar field with the following properties:

* It is the spatial part of the Steady State solution.
* Its magnitude has the meaning of Steady State amplitude field.
* Its phase has the meaning of Steady State relative phase field.

The information provided by $P$ is sufficient to fully characterise the physical steady state pressure disturbance $p$ a part for an overall unknown phase factor.

{{< cc >}}

