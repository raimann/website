---
title: "The FEM Pipeline"
date: 2018-10-28 20:00:00 +0100
draft: false
featured_image: "/posts_res/4-the-fem-pipeline/featured.svg"
description: "How to Build FEM Studies"
categories: ["Software"]
tags: ["Introductory", "Software"]
---

In the [previous article]({{< ref "/posts/0-modelling-acoustics-with-opensource-software.md" >}}) an overview of the open source ecosystem for scientific and technical computing was presented. In this article the focus will be on FEM. We will first look at how we build a FEM problem, and then select among the packages we listed the software that is best suited for implementing FEM studies.

# The Basic Pipeline

{{< include fname="static/posts_res/4-the-fem-pipeline/basic_pipeline.html" >}}

The diagram above shows the process of creating a FEM study in its simplest form. To create a FEM study, we first need a _problem_, that is, something we want to solve for. This means that we are interested in one (or more) portion of space, a _domain_, which represent a physical body that we think as governed by some PDE. When we have a single body, governed by a single equation, we are talking about a _single physics_ problem, while as soon as we have many governing equations, or many body interacting with one another, governed by different equations, we talk about _multiphysics_.

For example, the problem of the deformation of a metal bar is a single physics problem in which we create a bar (our domain) and we assign an equation modelling elasticity to it (the governing PDE). On the other hand, the problem of the wave radiated in air by a vibrating diapason is a multiphysics problem, where we model two domains (the diapason and the surrounding air), two different governing equations to each of them (elasticity and acoustics) and we _couple_ the two problems, that is, the acoustic equation act on the elasticity equation, by loading the vibrating body, while (simultaneously) the elasticity equation act on the acoustic equation (buy providing a field source term).

All of this will become clearer as we go along through the series. The important bit for now is that, once the problem is defined, the very first step is to represent all the bodies as geometric domains.

From then, the FEM pipeline is essentially composed of 3 steps:

1. Preprocessing.
2. Processing.
3. Postprocessing.

These steps are listed at the top of the diagram. They are looked in more detail below.

## Preprocessing

This step is composed by the following substeps:

* Geometry creation.
* Meshing.
* Study Setup.

The first two steps are self explanatory. In the setup study step the governing equations, boundary conditions and initial conditions are assigned, as well as all the solver settings (the solver settings are covered in more details in [this episode]({{< ref "/posts/9-elmerfem-model-and-solver-parameters.md" >}})).

For this phase different software packages are typically used. Normally, CAD packages are used to create the geometry. All the subsequent operations might be split between FEM suites and preprocessors in various ways. In this series the tasks will be handled by the packages listed at the bottom of the diagram.

## Processing

In this step the solver is run and the solution is computed. Once a problem is properly setup, this phase is as easy as sitting back and letting the computer do the job. However, it might me hard to set up a problem properly. More on this later.

## Postprocessing

After the problem is solved, it is time to look at the result. Postprocessing packages such as ParaView can be used, but raw data can be exported and manually processed with any technical computing language.

# The Development Pipeline

The basic pipeline described above constitutes the backbone of any FEM based pipeline. However, it is actually used as such only after the study development has been finished. In fact, getting a study up and running is harder than simply configuring some software and let the computer do some magic: it is important to understand whether we can expect the result to be accurate, and in what conditions. The use of the development pipeline below is aimed at building a model that can be trusted.

{{< include fname="static/posts_res/4-the-fem-pipeline/devel_pipeline.html" >}}

In order to tune a pipeline so that it delivers accurate results, it is useful to start from a simple problem, one for which an analytical solution exists. At various points along the pipeline it is possible to inspect various properties and use that to go back to the preprocessing step, in order to tune it more properly.

## Tuning with the Convergence Results

If during the solving step we face convergence issues, we should go back to the preprocessing. Very often convergence issues stem from the mesh, especially its order, but they might also stem by the use (or lack of use) of certain solver options. This will be covered throughout the series.

## Tuning with the Final Results

Once our solver has delivered results, we can compare them with the analytical ones, which act as a _benchmark_. If the agreement is poor, we know that we can go back and change something. This allows to figure out what handles there are on accuracy, and what improves it. Also this will be covered throughout the series.

## At the End of Tuning

Once the pipeline is tuned, and it provides good accuracy, we are free to plug more complex problems into its output, knowing that it should now be working fine since its output was compared with analytical solutions and delivered good results. This is, of course, easier said than done, as an excessively fine tuned pipeline on a particular model might not be very efficient or accurate on more complex models. It would be best to use few different benchmark models during development to ensure the pipeline is robust. Moreover, mutliphysics introduces a lot of complexity and that of convergence becomes a problem much harder to deal with. As the rest, we should cross that bridge when we get there.

# Conclusion

Solving problems with FEM is not trivial, and it normally involves some trial and error guided by simplified problems with known analytical solution. Whilst for simple problems optimal mesh and solver settings can be guessed, this is not the case for complex problems and development must be done before going full force through the basic pipeline. In other words, that of developing a FEM model is a scientific process that aims at raising the confidence of a modelling pipeline by putting it to test and critical scrutiny, until it performs satisfactory according to certain benchmark models.

In short: **never blindly trust the solver**, and make sure you develop your study right.

{{< cc >}}

