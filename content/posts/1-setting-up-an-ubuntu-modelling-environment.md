---
title:  "Setting Up an Ubuntu Modelling Environment"
date:   2018-10-27 21:00:00 +0100
draft: false
featured_image: "/posts_res/1-setting-up-an-ubuntu-modelling-environment/featured.png"
description: "Steps to Setup an Ubuntu FEM Workstation"
categories: ["Software"]
tags: ["Introductory", "Software", "Tutorial"]
---

In the [first episode]({{< ref "/posts/0-modelling-acoustics-with-opensource-software.md" >}}) of this series the open source ecosystem for the modelling of acoustics was covered. As we will see in the next episodes, we will make extensive use of FEM, at least initially, to model acoustic phenomena. This episode focus on building a simple simulation workstation based around ElmerFEM, a powerful multiphysics FEM suite with excellent acoustics capabilities.

# Why an Ubuntu Based Workstation?

[Ubuntu Linux](https://ubuntu.com/) is a very convenient operating system for this purpose as all the needed packages are straightforward to install, especially ElmerFEM and Salome, which can be quite complicated to get up and running in other distributions. This is the reason why this project uses Ubuntu as a base. Using an LTS version of Ubuntu is advised in order to get a stable working environment. Note that certain packages, such as Salome, often are released officially only for LTS versions.

It is possible, of course, to set any distribution to be a numerical modelling workstation. My operating system of choice is [Arch Linux](https://www.archlinux.org/), and there are also distributions focused on numerical modelling, such as [CAELinux](https://www.caelinux.com/CMS3/). If you do not like Ubuntu, my personal recommendation is to use some other reasonably mainstream distribution (Debian, Arch, OpenSuse, Gentoo, Fedora, CentOS...) and just install the software you need, rather than installing a specialised distribution. This allows you to take advantage of the extensive documentation of mainstream distributions, as well as the more active user forums should you need some help, and have a leaner workstation as a result.

# FreeCAD

It is best to install FreeCAD from the [FreeCAD stable releases PPA](https://launchpad.net/~freecad-maintainers/+archive/ubuntu/freecad-stable):

```bash
sudo add-apt-repository ppa:freecad-maintainers/freecad-stable
sudo apt-get update
sudo apt-get install freecad
```

This should pull in the latest stable FreeCAD version.

# Salome Platform

To install Salome Platform head to the [download page](http://www.salome-platform.org/downloads/current-version) and download the most appropriate version. Note that Salome versions for the most up to date Ubuntu LTS version might not be available, so it might be worth to install the most recent LTS version supported by Salome. Alternatively, universal Linux binaries _could_ be provided, depending on the version. How Salome is distributed depends on the version. Up to version 9.3.0 the universal binaries were distributed as `.run` files. Under Ubuntu, these files can be installed by running these commands in the directory where the universal binary archive has been downloaded:

```bash
chmod +x Salome-V8_5_0-univ_public.run
./Salome-V8_5_0-univ_public.run
```

Note that you will need to adapt the commands to match the filename of your downloaded file. The installer will ask a few questions. Answering the default by pressing enter should work fine.

From version 9.5.0 the universal Salome binaries are instead distributed as an archive that you can just extract in the most convenient location (see below additional notes for Arch Linux users).

Additional packages might be required in order to run Salome. For example, to have a working installation on Ubuntu 18.04 LTS I had to add `net-tools` and `libopengl0`:

```bash
sudo apt install net-tools libopengl0
```

## A Note for Arch Linux Users

For Salome version 9.3.0 you might try [this script](https://github.com/CrocoDuckoDucks/salome-arch-installer) to install the Salome universal binary on Arch Linux.

Salome 9.5.0 instead is distributed differently and does not need the script. However, the code needs a little "hack" to run on an up to date Arch Linux installation. If when running the `salome` launcher you get an error like this one:

```bash
subprocess.CalledProcessError: Command '['hostname', '--all-ip-addresses']' returned non-zero exit status 64.
```
you will be able to fix it by editing line 92 of `SALOME-9.5.0/BINARIES-CO7/KERNEL/bin/salome/nameserver.py` as follows:

```python
ips = check_output(['hostname', '--ip-addresses'])
```

# ElmerFEM

ElmerFEM can be very conveniently installed from the [Ubuntu PPA](https://launchpad.net/~elmer-csc-ubuntu/+archive/ubuntu/elmer-csc-ppa) with these commands:

```bash
sudo apt-get install libqt5xml5
sudo apt-add-repository ppa:elmer-csc-ubuntu/elmer-csc-ppa
sudo apt-get update
sudo apt-get install elmerfem-csc-eg
sudo cp -rs /usr/share/ElmerGUI/edf-extra/* /usr/share/ElmerGUI/edf/
```

The last command is meant to make available the various ElmerGUI elements that by default are disabled in the GUI. It could be best to run it after each system upgrade.

# Julia

To install Julia you should head to the [Julia download page](https://julialang.org/downloads/) and make sure you look at the [platform specific instructions](https://julialang.org/downloads/platform/). It is best to keep referring to the official documentation as things could change in the future. For recent Ubuntu distributions Julia should be included in the official repositories. So, installing should be as easy as running this command:

```bash
sudo apt install julia
```

Alternatively, you should be able to install Julia by using the `Ubuntu Software` application.

# ParaView

ParaView is available in the Ubuntu repositories. Simply install with `apt`:

```bash
sudo apt install paraview
```

# Conclusion

Setting up a numerical modelling workstation on Ubuntu is very simple: just run a few commands, wait for download, and then everything should up and running. On other distributions things might get more complicated and involve building from source, which is the reason why Ubuntu is recommended for setting up a FEM based workstation.

In the next episodes we will start using these packages to build models.

{{< cc >}}

