---
title: "FEM in a Nutshell"
date: 2018-10-27 23:00:00 +0100
draft: false
featured_image: "/posts_res/3-fem-in-a-nutshell/featured.png"
description: "A Brief Introduction to FEM"
categories: ["Modelling"]
tags: ["Introductory", "Physics"]
---

In the previous episodes we seen that physical systems are described by PDEs. However, PDEs are seldom solved analytically, unless for certain simple cases, the reason being that finding the solution of a PDE on a complex, realistic, domain with realistic boundary and initial conditions is extremely complicated, or downright impossible, even when the PDE is known to have analytical solutions from theoretical considerations. For this reason, numerical methods have arisen in order to handle these problems. The essence of numerical methods is that of creating a simplified problem, for which a solution can be found. Of course, the solution of the simplified problem is not the same as the solution of the original problem, but often numerical methods have _convergence_ properties, that is, they are controlled by parameters which, the bigger they are, the close the numerical solution is to the target one. These convergence properties are often demonstrated mathematically, which is very remarkable: although the real solution of a PDE is unknown, it is still possible to handle the problem knowing that a numerical solution can approach it within a certain tolerance.

In the previous episodes we also took the steps to create a FEM based workstation. This because FEM will be used to deal with many problems in this project. The FEM method should then be introduced before digging into real problems.

# The FEM Method

FEM is able to provide an approximated solution to our governing PDE by altering the _domain_ in which we apply it. The domain is no other than the portion of space in which we apply the PDE, i.e. the portion of space that represents the body we want to model. The first step of FEM is that of _meshing_: we cut the domain in many small not overlapping subdomains. They need not to have the same size or shape. At the vertices of these domains we put _nodes_, single points. For higher order meshes we will have nodes also inside. Then, we define basis functions over the inside of the domain, typically a family of polynomials whose order is linked to the mesh order. Simply speaking, the approximated solution to our problem is defined by a weighted sum of these basis functions, and the weights are found by solving matrix equations involving the value of the unknown field at the nodes, which is then the real unknown of the problem. Given that the number of nodes is then the number of unknowns, this number is also called the number of _degrees of freedom_.

An example of meshed domain, a cube, is shown below. The first picture represent the whole domain, while in the second one the meshed domain was cut through to show the inside mesh.

{{< figure src="/website/posts_res/3-fem-in-a-nutshell/meshed_cube.png" title="Figure 1" caption="Meshed Domain (whole)" class="mw10" >}}

{{< figure src="/website/posts_res/3-fem-in-a-nutshell/meshed_cube_clipped.png" title="Figure 2" caption="Meshed Domain (clipped)" class="mw10" >}}


In essence, FEM works by simplifying a problem over infinite points (all the points inside the domain) to a problem over a finite set of points (the nodes). Or, in other words, it turns an infinitely dimensional problem into a finite dimensional one. However, thanks to the basis functions, the solution is actually defined everywhere in the original domain through interpolation: FEM still provides a solution defined everywhere in the domain.

The typical way to turn a PDE into a FEM scheme is as follows:

1. The PDE is converted in a weak form which makes use of integral formulation. This formulation is completely equivalent to the original PDE, and it is not any easier to solve.
2. The domain is discretised through meshing. **This** is the step that introduce the simplification of the problem.
3. It is imposed that, in the mesh _elements_, the unknown field is represented by a linear combination of basis functions.
3. As a consequence, the weak form equation turns into a matrix equation for each element, involving the value of the unknown field at the nodes.
4. The matrix equations for each element are assembled into a big matrix equation for the whole domain.
5. The matrix equation is solved by using linear algebra methods (if the boundary conditions are provided).

This might seem very complicated at first, and in fact it is quite advanced mathematics, but the important takeaway here are these:

- **Given that meshing is the process at the very core of the FEM method, through which the simplification itself is achieved, we should make it right.**

- **Accuracy is mainly linked to the quality of the meshing**

Linear algebra techniques to solve big matrix problems are not trivial. For simple problems direct methods can be used, but for big problems the linear system must be solved through iterations (this will be covered in later episodes). A bad meshing can produce numerical values during the solution that are hard to deal with for computers, and introduce many errors or convergence problems. A too coarse meshing is simply incapable of supplying a realistic solution. Hence: meshing is one of the most important steps, and most of the time you will found yourself working on it in order to increase the accuracy of your simulation.

# The Meanings of Convergence

We touched on the word _convergence_ a couple of times, but what that means in the context of FEM? It can have essentially two meanings:

1. The convergence of the simplified solution to the original PDE solution as the mesh is _refined_.
2. The convergence of the iterative linear system iterations.

These are both advanced topics. For point 1, it is possible to demonstrate that, for single physics problems, the FEM solution converges to the original PDE solution the smaller the mesh size and the higher its order. However, there are downsides to using very high order and very high mesh density. First, they make the simulation computational requirements to blow. Second, they might actually make the second type of convergence harder.

For point 2, we mean something different. If FEM is trying to solve a big linear system (i.e matrix equations with big matrices) through iterations, we have to know when to stop the iterations. Or, we need to have a _stopping criterion_ to understand when we can stop iterating, our solution having reached a good approximation of what would be the direct linear system solution (if we could solve it directly). A good way to do this is by comparing the solution at the current iteration to the previous one. If they are very similar, we know that we reached the final solution, as the iterations are not producing any new change of the result we got.

So, there are two convergence threads going on:

* The simplified FEM problem, whose solution is converging to the original PDE the finer and/or higher order the mesh is.
* The linear system solution which, if we use an iterative algorithm, is converging to the real linear system solution the more we keep on iterating.

## What About Multiphysics?

For multiphysics the FEM convergence is harder to deal with, as there isn't a general convergence theorem and problems have to be dealt with on a case-by-case basis.

## What About Nonlinear Systems?

If our PDE is nonlinear, FEM will still produce linear systems. In order to model nonlinearity, we have to use another iterative approach in which we solve many linear system one after the other in order to approximate a nonlinear system. Similar convergence considerations as above apply to this _nonlinear iterations_.

# Conclusion

This was a very very brief introduction to FEM and it only covered the very basics. However, we now know that:

* FEM simplifies a PDE defined over a domain by dividing the domain in elements.
* The elements tile the domain without overlaps or gaps.
* The element communicates only through nodes.
* Higher order meshes have nodes also within the elements.
* The field at the nodes defines the field within the element through interpolation by making use of a set of basis functions.
* For single physics, the FEM solution converges to the PDE solution the finer the mesh and/or the higher its order.
* Fine and high order meshes raise computational costs.
* The linear systems produced by FEM can be so big that cannot be solved directly and need iterative algorithms. The iterations are stopped when the linear system solution is not varying anymore, i.e. when it has converged.
* Fine and high order meshes can make linear system iterative convergence hard to achieve.
* Nonlinear systems are dealt with by iterating between linear systems, until the solution converges.
* Multiphysics problems convergence is harder to deal with.

All of the above tells us that when we work with FEM we really should monitor our solution and convergence to ensure that things are really getting more accurate, as clearly there are few conflicting requirements above. Solving a problem with FEM requires a lot of scientific and critical thinking, and we need to craft a solution by putting it under constant scrutiny and test. We will see how to do this in the next episodes.

# Further Reading

For an overall gentle introduction in the modern applications of FEM in acoustics, check the article [Solving Complex Acoustic Problems Using High-Performance Computations](https://acousticstoday.org/solving-complex-acoustic-problems-using-high-performance-computations-gregory-bunting/) by Gregory Bunting et al.

There are many online resources if you want to lookup more info on FEM and meshing. One that I would recommend is the [COMSOL's multiphysics cyclopedia](https://www.comsol.com/multiphysics)

The [ElmerFEM](http://www.elmerfem.org/blog/documentation/) documentation is also packed with very good information.

Finally, I can recommend [this course](https://www.coursera.org/learn/finite-element-method/) if you want to get your hand dirty with FEM and learn the basic with higher detail. Note that material from the course is also published by the University of Michigan on [YouTube](https://www.youtube.com/watch?v=U65GK1vVw4o&list=PLJhG_d-Sp_JHKVRhfTgDqbic_4MHpltXZ).

{{< cc >}}

