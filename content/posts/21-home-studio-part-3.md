---
title: "Home Studio - Part 3"
date: 2020-12-05T13:55:12Z
draft: false
featured_image: "/posts_res/21-home-studio-part-3/featured.png"
description: "High Resolution Low Frequency Reponse"
categories: ["Modelling"]
tags: ["Physics", "ElmerFEM", "FEM", "Linear System", "Single Physics", "Steady State", "Frequency Response"]
---

In the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode we gave a first look at the response of a realistically shaped room with rigid walls. We understood how to develop and run a model with an uniform velocity sphere source placed somewhere in the room. We run the model up to $400$ $\text{Hz}$ with the help of the convergence considerations outlined in the [Dealing with Convergence Issues]({{< ref "/posts/13-dealing-with-convergence-issues.md" >}}) episode. In the [Home Studio - Part 2]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode instead we used Elmer's `WaveSolver` to compute the eigenmodes of the room. Not only that, but we introduced several improvements such as the use of $p$-elements and locally refined meshes. In this episode we will model in high detail the low frequency response of our room, up to $125$ $\text{Hz}$, making use of the improvements we just mentioned.

# Why Focusing Only up to $125$ $\text{Hz}$

Low frequency problems are easier to handle with FEM. In this and the next few episodes we will explore the methodologies to compute the frequency response of acoustic systems. In later episodes we will be able to extend these methods to compute the same quantities at higher frequencies.

# Project Files

All the files used for this project are available at the repositories below:

* [Home Studio 5](https://gitlab.com/computational-acoustics/home-studio-5).

# The Meshing

The meshing is performed with Salome by following our typical paradigm, i.e. `NETGEN 1D-2D-3D` with a maximum mesh size $s$ such that:

$$ s < \frac{\lambda}{10} = \frac{c}{10f}$$

where $c$ is the phase speed of sound ($343$ $\frac{\text{m}}{\text{s}}$ for air in room temperature, as in this simulation), $f$ is the highest frequency at which we want to run the simulation ($125$ $\text{Hz}$) and $\lambda$ is its associated wavelength. In this episode we will choose the value of $220$ $\text{mm}$ for $s$, which allows us to simulate up to $155.91$ $\text{Hz}$, thus giving us a little bit of a safety margin.

Since the `HelmholtzSolve` supports $p$-elements we will also handle the element order from within Elmer, as we did in the [Home Studio - Part 3]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode. So we will keep the mesh as a first order mesh within Salome, and set the `Element` keyword in the Solver section to `p:2` to specify second order.

The mesh settings are shown in the picture below.

{{< figure src="/website/posts_res/21-home-studio-part-3/mesh-settings-1.png" title="Figure 1" caption="Mesh Settings (Arguments)" class="mw6" >}}

Similarly to what we did in the [Home Studio - Part 3]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode we will use local a $50$ $\text{mm}$ refinement on the door and entrance area, as well as the window. We will use local refinement also on the sphere source, with a local size of $1$ $\text{mm}$. See the [Home Studio - Part 3]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode for more information about local refinement.

{{< figure src="/website/posts_res/21-home-studio-part-3/mesh-settings-2.png" title="Figure 2" caption="Mesh Settings (Local sizes)" class="mw6" >}}

As we did in the past few episodes, we will use [salomeToElmer](https://github.com/mrkearden/salomeToElmer) to export our mesh, so it is a good idea to name all the volume and face mesh groups to ease the assignment of the boundary conditions later on, as shown below.

{{< figure src="/website/posts_res/21-home-studio-part-3/mesh-groups.png" title="Figure 3" caption="Mesh Group Names" class="mw6" >}}

The computed mesh is shown in the picture below. It is possible to see the local refinement in action on the entrance, door and window.

{{< figure src="/website/posts_res/21-home-studio-part-3/mesh.png" title="Figure 4" caption="Mesh" class="mw10" >}}

The figure below shows the mesh clipped along the $x$ axis, with a plane passing through the source. The entrance area is on the left of the picture. It is possible to appreciate the effects of local refinement, both on the entrance and the source. Despite the refinements being defined on external faces their effects "spread" into the inner volume, creating regions of higher mesh density, exactly as we wish.

{{< figure src="/website/posts_res/21-home-studio-part-3/mesh-clip.png" title="Figure 5" caption="Internal Mesh Details" class="mw10" >}}

For more details, you can refer to the [Salome study](https://gitlab.com/computational-acoustics/home-studio-5/-/blob/master/meshing.hdf) in the repository as well to the previous episodes for step-by-step guidance on how to create meshes.

# ElmerFEM Study Setup

The study is fairly straightforward to setup, however we will set it up in the "advanced" way, that is, by creating the solver input file (`sif`) as we did for the various _Rigid Walled Room Revisited -_ episodes. This is actually very easy to do, as the study is very similar to that of the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, so we can simply edit the `sif` file from that study.

Once you export the mesh as in the instructions provided by [salomeToElmer](https://github.com/mrkearden/salomeToElmer) (the mesh will be most likely exported in the Salome home folder in your PC) you can proceed to move all the mesh files to the directory where you will run Elmer. The process of exporting meshes with [salomeToElmer](https://github.com/mrkearden/salomeToElmer) is explained in details in the [Rigid Walled Room Revisited - Part 1]({{< ref "/posts/15-rigid-walled-room-revisited-part-1.md" >}}) episode.

Next, you will need create a solver input file in the same directory. As mentioned, the study is very similar to that of the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, the main difference being that we will use $p$-elements and do a high resolution `Scanning` simulation from $1$ $\text{Hz}$ to $125$ $\text{Hz}$ in $1$ $\text{Hz}$ steps, but by making sure we include in the simulation also the resonance frequencies computed in the [Home Studio - Part 3]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode, so that we can see what happens when we drive a room at resonance. The solver input file will then look very similar to that of the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, just with many more frequencies.

One good way to compute the frequencies is as follows. Launch Julia and hit the `;` key. This will make the REPL to act as a terminal. If you cloned and solved the study of the [Home Studio - Part 3]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode navigate to its root folder. Then, run the following lines:

```julia
include("fea-tools/tools.jl")
f = read_fem_eigenfrequencies("elmerfem/eigenvalues.dat")
F = 1:125
d = sort(unique(vcat(f, F)))
d = transpose(d[d .> 0])
```

I find then useful to hit `;` again and navigate to where I will run Elmer for the current study. At this point we can save the frequencies to file as follows:

```julia
using DelimitedFiles
writedlm("elmerfem/frequencies.txt", array, ' ')
```

Finally, you can check how many frequencies we have to solve for with:

```julia
length(d)
```

In the `Simulation` section of your solver input file you can then set the keywords as follows:

```text
Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Coordinate Mapping(3) = 1 2 3
  Simulation Type = Scanning
  Steady State Max Iterations = 1
  Output Intervals = 1
  Timestepping Method = BDF
  BDF Order = 1
  Timestep intervals = 134
  Coordinate Scaling = 0.001
  Solver Input File = case.sif
  Post File = case.vtu
$ f = 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0 21.0 22.0 23.0 24.0 25.0 26.0 27.0 28.0 29.0 30.0 31.0 32.0 33.0 34.0 35.0 36.0 37.0 38.0 38.87460581286569 39.0 40.0 41.0 42.0 43.0 44.0 45.0 46.0 47.0 48.0 49.0 50.0 51.0 52.0 53.0 54.0 55.0 56.0 57.0 58.0 59.0 59.944517111825824 60.0 61.0 62.0 63.0 64.0 65.0 66.0 67.0 68.0 69.0 70.0 71.0 71.12338044621859 72.0 73.0 74.0 75.0 76.0 76.2226096881928 77.0 78.0 78.74873387812008 79.0 80.0 81.0 82.0 83.0 84.0 85.0 85.5638895654582 86.0 87.0 88.0 89.0 90.0 91.0 92.0 93.0 94.0 95.0 96.0 96.97157816943052 97.0 98.0 99.0 99.26649696726423 100.0 101.0 102.0 103.0 104.0 104.25320323657283 105.0 106.0 107.0 108.0 109.0 110.0 111.0 112.0 113.0 114.0 115.0 116.0 117.0 118.0 119.0 120.0 121.0 122.0 123.0 124.0 125.0
$ p = 1.205
$ U = 10
End
```

Where the MATC array `f` was easily filled by opening the `frequencies.txt` file created above and copying the contents in there. Note the value of `Timestep intervals`, which is set to the length of the `d` array computed with Julia. Recall that `p` is the density of air at room temperature, and `U` is the surface velocity of the sphere source (all in SI units).

The `Solver` section of the input solver file is the same as for the [Home Studio - Part 1]({{< ref "/posts/12-home-studio-part-1.md" >}}) episode, just with the addition of the $p$-element settings for second order elements:

```text
Element = "p:2"
```

We will use just two boundary conditions: a rigid one for all boundaries and a radiating one for the sphere source. They are shown below:

```text
Boundary Condition 1
  Target Boundaries(1) = 6 
  Name = "Radiator"
  Wave Flux 2 = Variable time; Real MATC "2 * pi * f(tx - 1) * p * U"
End

Boundary Condition 2
  Target Boundaries(10) = 2 3 4 5 7 8 9 10 11 12 
  Name = "Rigid"
  Wave Flux 1 = 0
  Wave Flux 2 = 0
End
```

The figure out the target boundaries you can refer to the `mesh.names` file which, if you named the mesh groups as recommended above, will read as follows:

```text
! ----- names for bodies -----
$ air = 1
! ----- names for boundaries -----
$ wall_left = 2
$ door = 3
$ wall_entrance_back = 4
$ wall_entrance_right = 5
$ source = 6
$ floor = 7
$ ceiling = 8
$ wall_front = 9
$ wall_back = 10
$ window = 11
$ wall_right = 12
$ empty = 13
```
The full `sif` file is available [here](https://gitlab.com/computational-acoustics/home-studio-5/-/blob/master/elmerfem/case.sif) in the study repository.

Finally, just create the `ELMERSOLVER_STARTINFO` file with the following content:

```text
case.sif
1
```

Now Elmer will run the study every time you run the command `ElmerSolver` in the study directory.

# The Solution

## The Field

{{< youtube MwXjIPuusZA >}}

The video above shows the Sound Pressure Level (SPL) of the solution on the left, and phase field of the solution on the right. For an overview of how these properties are computed, refer to the [Interpreting Helmholtz Solver Solutions]({{< ref "/posts/18-interpreting-helmholtz-solver-solutions.md" >}}) episode.

Since the resonance frequencies of the room are important to understand the results they are reported below, as computed in in the [Home Studio - Part 2]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode:

{{< load-table-dfl-style >}}
| Mode Number | Mode Frequency \[Hz\] |
|-------------|-----------------------|
|1            | 0                     |
|2            | 38.87                 |
|3            | 59.94                 |
|4            | 71.12                 |
|5            | 76.22                 |
|6            | 78.75                 |
|7            | 85.56                 |
|8            | 96.97                 |
|9            | 99.27                 |
|10           | 104.25                |

The first thing that is evident is that, at frequencies below the first nontrivial mode ($38.87$ $\text{Hz}$) there is not a whole lot of "structure" in the field, that is, the SPL is constant all over the room, only varying nearby the source. The lower the frequency the more uniform the field. In this regime the air within the room is _lumped_, that is, the air is moving as a whole. Some non-uniformity (a sign of _distributed_ behaviour) starts appearing at around $20$ $\text{Hz}$. At $38.87$ $\text{Hz}$ the field is definitely distributed instead, with strong spatial dependency.

It is possible to see that there is a severe surge in SPL every time the frequency matches a resonance frequency. This is expected, as the resonance frequencies are frequencies at which energy transfer is maximised. For all other frequencies the field SPL is way lower.

As we noted in the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) when a modal system (like a room) is driven by a source multiple modes get activated. However, at low resonance frequencies, where the modes are far from one another, the contributions from other modes are negligible. We can clearly see that here too, as the shapes  of the SPL fields match with the modal shapes found in the [Home Studio - Part 2]({{< ref "/posts/19-home-studio-part-2.md" >}}) episode. However, the modal superposition is evident at frequencies in between the modes, where it can be seen that the shape of the field is "intermediate" between two modes.

Finally, the phase field is uniform while the system is lumped, while it is cut in multiple regions of opposite sign when the system is distributed. This means that the air volume is segmented in pieces all vibrating either in phase (same colour) or with opposite phase (different colour). To be noted that the phase values are either $\frac{\pi}{2}$ or $-\frac{\pi}{2}$, pretty much constant within the segment.

## Frequency Response at Two Different Locations

It is interesting to take a couple of points in the domain and extract the field values as a function of frequency at those points. To do so, open the data with ParaView as per usual and use the `Probe Location` filter (for more information about ParaView see the [Intro to ParaView]({{< ref "/posts/11-intro-to-paraview.md" >}}) episode). You will be able to configure the probe location under _Sphere Parameters_ in the _Properties_ manager. I used two different point sources, one near the window and one near the door. The coordinates are as follows:

| Probe           | $x$ $\text{[m]}$ | $y$ $\text{[m]}$ | $z$ $\text{[m]}$ |
|-----------------|------------------|------------------|------------------|
| 1 (near window) | $2.5$            | $3.5$            | $1.7$            |
| 2 (near door)   | $-0.5$           | $3.2$            | $1.7$            |

Then, you can select the probe object from the _Pipeline Browser_ and choose _File_ from the top menu and then _Save Data_. Specify a `csv` file name anywhere you like, and then make sure you save all time steps with appropriate precision, as shown below.

{{< figure src="/website/posts_res/21-home-studio-part-3/save-data.png" title="Figure 7" caption="Save Data Dialog" class="mw7" >}}

Once the data is saved, it can be imported into Julia and further analysed (see [this code](https://gitlab.com/computational-acoustics/home-studio-5/-/blob/master/make_response.jl) in the repository as an example).

In the plot below the magnitude of the field recorded at the two probes, divided by the source surface velocity `U` ($10$ $\frac{\text{m}}{\text{s}}$), is reported as a function of frequency in units of SPL per unit velocity. This curve has the meaning of linear frequency response magnitude from source velocity to pressure.

{{< include fname="static/posts_res/21-home-studio-part-3/responses.html" >}}

[Click Here for Larger Version](/website/posts_res/21-home-studio-part-3/responses.html)

The resonance frequencies (with exception of the uninteresting $0$ $\text{Hz}$ mode) are shown as vertical dashed lines for reference. It is possible to see that there are distinctive peaks at the resonance frequencies. The peaks are very tall and narrow. Numerically the peak values at the two probe locations are very similar, but not equal. More dramatic is the difference, between the two probes, of the anti-resonances, i.e. the valleys between the peaks.

Frequency responses of this kind, a series of resonances and anti-resonances, are typical of modal systems, of which rooms are a typical example. At low frequency, as in this case, the peaks are aligned with the modal frequencies. At high frequencies however the modes will be so closely packed together (see the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) episode), and so strongly interacting with each other, that the response might have dips also where resonance frequencies are.

In any case, we can see that the response at a given point is not equal to the response in another point. This has to be expected given that the field has such a strong dependency on the spatial variables. As we observed in the [Rigid Walled Room]({{< ref "/posts/10-rigid-walled-room.md" >}}) the room behaviour also changes if the source is placed in another position within the room. Hence we conclude that a room doesn't have _a_ response. Instead it has _infinite_ responses, depending on:

* The type of source(s) and its position.
* The type of receiver(s) and its position.

In this case we are using one uniform velocity omnidirectional source and one ideal omnidirectional probe. Even in this very simple case we will have a different response for any given pair of source-probe position.

This allows to appreciate why room treatment is typically very important for listening spaces. Whilst ideally rigid boundaries, like those in this simulation, do not exist many materials used in construction are actually very reflective. The responses of untreated rooms might then look very similar to the ones reported above. This is undesirable as certain frequencies will be overly emphasised by the room, with other being overly attenuated, preventing neutral reproduction of sound. The higher the absorption and scattering at the room walls the more the resonant response will be smoothed, yielding to a more neutral response. This is why room treatment typically focuses on the wall, and makes use of absorptive panels and scattering panels.

# Conclusion

In this episode we produced a high resolution low frequency sweep of a realistically shaped room with ideally rigid boundaries. We used locally refined meshes and second order $p$-elements. What we found is a highly modal room response already at frequencies in the order of $30$ $\text{Hz}$, where the first modal anti-resonances can be seen.

In the next episodes we will make the model more realistic by incorporating real materials impedance in the boundary conditions.

{{< cc >}}


