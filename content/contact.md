---
title: Contact
featured_image: "/site_img/featured_image.png"
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main

---

Thank you for wanting to reach out, your feedback on this project is very valuable. Please take a look at the following before filling the contact form.

* **I Found a Bug/Issue on the Website.** If this is the case, please file an issue [here](https://gitlab.com/computational-acoustics/website/-/issues).

* **I Found a Bug/Issue on the Various Projects Repositories.** In this case, please file an issue in the most appropriate [repository](https://gitlab.com/computational-acoustics).

* **I Want to Contribute to the Project.** Thank you, that's ace! Unfortunately access requests to the project cannot be granted at the moment, as I am not in the position to manage multiple developers and review their activity. If you wish to contribute to [existing projects](https://gitlab.com/computational-acoustics) perhaps the best way is to [fork one](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html). If you wish to add an existing project to the group please be in touch by filling the form below.

For anything else, please fill the form below.

{{< form-contact action="https://formspree.io/mrgylkdo" >}}

