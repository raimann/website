#!/usr/bin/env julia

include("../../../web-plots/web-plots.jl")

c = 343
f = 10 .^ range(0, 5, length=100)

s = c ./ (10 .* f)

fig = canvas(BasicConfig("../../../config.toml"))

fig.update_layout(
    xaxis_title="Frequency [Hz]",
    yaxis_title="Maximum Mesh Size [m]"
    )

fig.update_xaxes(type="log")
fig.update_yaxes(type="log")

add_line!(fig, x=f, y=s, mode="lines")

save_plot_with_script(fig, "s_plot.html")
