#!/usr/bin/env julia

using JLD
using DelimitedFiles

include("../../../web-plots/web-plots.jl")

eigv = readdlm("eigenvalues.dat")[:]
eigf = sqrt.(eigv[eigv .> 0]) ./ 2π
U = 10  # Source velocity as in sif.
ref = U * 20e-6 # Reference for dB re uPa/(m/s)

data = load("response.jld")

fig = canvas(BasicConfig("../../../config.toml"))

fig.update_layout(
    xaxis_title="Frequency [Hz]",
    yaxis_title="Magnitude [dB re 20 μPa / (m/s)]"
    )
    
add_line!(
    fig,
    x=data["frequencies"],
    y=20 * log10.(abs.(data["response1"] ./ ref)),
    mode="lines",
    name="Probe 1"
    )
    
add_line!(
    fig,
    x=data["frequencies"],
    y=20 * log10.(abs.(data["response2"] ./ ref)),
    mode="lines",
    name="Probe 2"
    )
    
for f in 1:length(eigf)
    add_line!(
        fig,
        x=[eigf[f], eigf[f]],
        y=[0, 200],
        mode="lines",
        line=Dict("dash" => "dash"),
        name="Mode $(f + 1)"
        )
end

fig.update_layout(yaxis_range=[0, 200])
    
save_plot_with_script(fig, "responses.html")

