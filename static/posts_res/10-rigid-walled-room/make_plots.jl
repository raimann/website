#!/usr/bin/env julia

using JLD

include("../../../acoustic-models/models.jl")
include("../../../web-plots/web-plots.jl")

Lx = 5.0
Ly = 4.0
Lz = 3.0

A, B, C = indexGrid(100, 100, 100)
F = modeFrequency.(A, B, C, Lx, Ly, Lz)[:]
sel_idx = F .<= 1e3
f = sort(F[sel_idx])
c = 1200 .* log2.(f[2:end] ./ f[1:(end - 1)])
n = collect(1:length(f))

bc = BasicConfig("../../../config.toml")

fig_modal = subplots(bc, rows=2, cols=1, shared_xaxes=true)

add_line_subplot!(
    fig_modal,
    1,
    1,
    x=n,
    y=f,
    name="Modal Frequency",
    mode="lines"
    )

add_line_subplot!(
    fig_modal,
    2,
    1,
    x=n,
    y=c,
    name="Modal Frequency <br> Distances",
    mode="lines"
    )

fig_modal.update_xaxes(type="log")
fig_modal.update_xaxes(title_text="Mode Number", row=2, col=1)
fig_modal.update_yaxes(title_text="[Hz]", row=1, col=1)
fig_modal.update_yaxes(title_text="[cents]", row=2, col=1)

save_plot_with_script(fig_modal, "modal_behavior.html")

validation_data = load("validation_data.jld")

fig_norms = canvas(bc)

add_bar!(
    fig_norms,
    x=collect(1:length(validation_data["norms"])),
    y=validation_data["norms"]
    )

fig_norms.update_xaxes(
    title_text="Driving Frequency [Hz]",
    ticktext=string.(round.(validation_data["f"], digits=2)),
    tickvals=collect(1:length(validation_data["norms"]))
    )

fig_norms.update_yaxes(
    title_text="Norm [Pascals per Cubic Meter]",
    type="log"
    )

save_plot_with_script(fig_norms, "error_field_norm.html")
